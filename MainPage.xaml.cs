﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;

//let's see if we can't get SQLite working huh
//using System.Data.SQLite;

// The Basic Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234237

namespace Jester
{
    /// <summary>
    /// A basic page that provides characteristics common to most applications.
    /// </summary>
    public sealed partial class MainPage : Jester.Common.LayoutAwarePage
    {

        private Dictionary<String, String> boatDict = new Dictionary<String, String>();
        private String boatKey;
        private int selectedIndex;

        public MainPage()
        {
            this.InitializeComponent();
           
        }

        private void ParseData()
        {
            //stub: parse database and store title as key and id as value
           

        }
        /// <summary>
        /// Populates the page with content passed during navigation.  Any saved state is also
        /// provided when recreating a page from a prior session.
        /// </summary>
        /// <param name="navigationParameter">The parameter value passed to
        /// <see cref="Frame.Navigate(Type, Object)"/> when this page was initially requested.
        /// </param>
        /// <param name="pageState">A dictionary of state preserved by this page during an earlier
        /// session.  This will be null the first time a page is visited.</param>
        protected override void LoadState(Object navigationParameter, Dictionary<String, Object> pageState)
        {
            DataStore jesterData;
            if (pageState == null)
            {
                jesterData = DataStore.Instance;
                boatDict = jesterData.getBoats();
                for (int i = 0; i < boatDict.Count; i++)
                {
                    GridViewItem boatGridItem = new GridViewItem
                                            {
                                                Background = new SolidColorBrush(Colors.Gray),
                                                Content = boatDict.ElementAt(i).Key,
                                                Width = 350,
                                                Height = 150,
                                                IsDoubleTapEnabled = true,
                                            };
                    boatNames.Items.Add(boatGridItem);
                }
            }
            else
            {
                if (pageState.ContainsKey("boatDict"))
                {
                    boatDict = (Dictionary<String, String>)pageState["boatDict"];
                    for (int i = 0; i < boatDict.Count; i++)
                    {
                        GridViewItem boatGridItem = new GridViewItem
                                           {
                                               Background = new SolidColorBrush(Colors.Gray),
                                               Content = boatDict.ElementAt(i).Key,
                                               Width = 350,
                                               Height = 150,
                                               IsDoubleTapEnabled = true,
                                           };
                        boatNames.Items.Add(boatGridItem);
                    }
                }
                else
                {
                    jesterData = DataStore.Instance;
                    boatDict = jesterData.getBoats();
                    for (int i = 0; i < boatDict.Count; i++)
                    {
                        GridViewItem boatGridItem = new GridViewItem
                                            {
                                                Background = new SolidColorBrush(Colors.Gray),
                                                Content = boatDict.ElementAt(i).Key,
                                                Width = 350,
                                                Height = 150,
                                                IsDoubleTapEnabled = true,
                                            };
                        boatNames.Items.Add(boatGridItem);
                    }
                }
                if (pageState.ContainsKey("boatKey"))
                    boatKey = pageState["boatKey"].ToString();
                if (pageState.ContainsKey("selectedIndex"))
                {
                    boatNames.SelectedIndex = (int)pageState["selectedIndex"];
                    selectedIndex = boatNames.SelectedIndex;
                }

            }
        }

        /// <summary>
        /// Preserves state associated with this page in case the application is suspended or the
        /// page is discarded from the navigation cache.  Values must conform to the serialization
        /// requirements of <see cref="SuspensionManager.SessionState"/>.
        /// </summary>
        /// <param name="pageState">An empty dictionary to be populated with serializable state.</param>
        protected override void SaveState(Dictionary<String, Object> pageState)
        {
            //stub: save dictionary, selected item
            pageState["boatDict"] = boatDict;
            if (boatKey != null)
            {
                pageState["boatKey"] = boatKey;
                pageState["selectedIndex"] = selectedIndex;
            }
        }

        //When a boat name is chosen, the specs picture on the app is changed
        private void Grid_SelectionChanged_1(object sender, SelectionChangedEventArgs e)
        {
            //this should work if everything is parsed and put in Dictionary
           
            boatKey = boatNames.SelectedItem.ToString();
            selectedIndex = boatNames.SelectedIndex;

            boatKey = ((GridViewItem)boatNames.SelectedItem).Content.ToString();

            String selectedID = boatDict[boatKey];
            var uri = new System.Uri("ms-appx:///Assets/media/"+selectedID+"/specs.png");
            Specs.Source = new BitmapImage(uri);
            ImageBrush myBrush = new ImageBrush();
            myBrush.ImageSource = new BitmapImage(new System.Uri("ms-appx:///Assets/media/" +selectedID+"/specs.png"));
            Background = myBrush;
        }

        //When the specs picture is double tapped, the page moves to the Details page of the chosen boat
        private void Spec_DoubleTap(object sender, DoubleTappedRoutedEventArgs e)
        {
            if (this.Frame != null)
            {
                if (boatKey != null)
                    this.Frame.Navigate(typeof(DetailsPage), boatKey);
            }
        }
    }
}
