﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;

// The Basic Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234237

namespace Jester
{
    /// <summary>
    /// A basic page that provides characteristics common to most applications.
    /// </summary>
    public sealed partial class ContentDisplay : Jester.Common.LayoutAwarePage
    {
        private FlipInfo myInfo = new FlipInfo();
        private int contentIndex;
        private List<String> mediaIDs = new List<String>();
        public ContentDisplay()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Populates the page with content passed during navigation.  Any saved state is also
        /// provided when recreating a page from a prior session.
        /// </summary>
        /// <param name="navigationParameter">The parameter value passed to
        /// <see cref="Frame.Navigate(Type, Object)"/> when this page was initially requested.
        /// </param>
        /// <param name="pageState">A dictionary of state preserved by this page during an earlier
        /// session.  This will be null the first time a page is visited.</param>
        protected override void LoadState(Object navigationParameter, Dictionary<String, Object> pageState)
        {
        }
       
        //retrieve title using id sent from previous Frame
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            //retrieve title using id sent from previous Frame.
            myInfo = e.Parameter as FlipInfo;
            mediaIDs = myInfo.getMediaStrings();
            contentIndex = myInfo.getStartIndex();
            List<Image> ThumbnailSource = new List<Image>();
            for (int i = 0; i < mediaIDs.Count; i++)
            {
                Image temp = new Image();
                var uri = new System.Uri("ms-appx:///Assets/media/" + mediaIDs[i] + "/__default@2x.jpg");
                temp.Source = new BitmapImage(uri);
                ThumbnailSource.Add(temp);
            }

            //try using listview and imagesource
            DisplayImage.ItemsSource = ThumbnailSource;
            DisplayImage.SelectedIndex = contentIndex;

           

        }
        /// <summary>
        /// Preserves state associated with this page in case the application is suspended or the
        /// page is discarded from the navigation cache.  Values must conform to the serialization
        /// requirements of <see cref="SuspensionManager.SessionState"/>.
        /// </summary>
        /// <param name="pageState">An empty dictionary to be populated with serializable state.</param>
        protected override void SaveState(Dictionary<String, Object> pageState)
        {
        }
    }
}
