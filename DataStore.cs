﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Data.Json;
using Windows.Storage;
using System.Diagnostics;

namespace Jester
{
    public sealed class DataStore
    {
        //this could probably be private I would think
        public static readonly DataStore instance = new DataStore();

        public enum JesterMediaTypes
        {
            JMTTour = 0, JMTPhotos, JMTDrawings, JMTBrochure, JMTSpecifications, JMTVideos, JMTArticles, JMTReviews
        }

        private String DBPath = "marine.sqlite";

        private Dictionary<String,JsonValue> boatData;
        private Dictionary<String, JsonValue> mediaData;

        private DataStore(){
            //setup database connection
            

            using (var db = new SQLite.SQLiteConnection(this.DBPath,SQLite.SQLiteOpenFlags.ReadOnly,true)){
                boatData = new Dictionary<string, JsonValue>();
                mediaData = new Dictionary<string, JsonValue>();

                IEnumerable<BoatData> bdE = db.Query<BoatData>("select * from boat");
                foreach (BoatData bd in bdE)
                {
                    boatData.Add(bd._id, JsonValue.Parse(bd.doc));
                }

                IEnumerable<MediaData> mdE = db.Query<MediaData>("select * from media");
                foreach (MediaData md in mdE)
                {
                    mediaData.Add(md._id, JsonValue.Parse(md.doc));
                }
            }
        }

        public static DataStore Instance{
            get {
                return instance;
            }
        }

        public Dictionary<String, String> getBoats()
        {
            Dictionary<String, String> retVal = new Dictionary<String, String>();

            foreach (KeyValuePair<String, JsonValue> pair in this.boatData)
            {
                retVal.Add(pair.Value.GetObject().GetNamedString("title"), pair.Key);
            }

            return retVal;
        }

        public Dictionary<String, List<String>> getBoatMedia(String boatID)
        {
            Dictionary<String, List<String>> retVal = new Dictionary<string, List<string>>();

            JsonObject obj = this.boatData[boatID].GetObject();
            JsonArray mediaList = obj.GetNamedArray("mediaCollections");
            for (uint i = 0; i < mediaList.Count; i++)
            {
                List<String> mediaIDs = new List<String>();
                JsonArray vals = mediaList.GetObjectAt(i).GetNamedArray("media");
                for (uint x = 0; x < vals.Count; x++)
                {
                    mediaIDs.Add(vals.GetStringAt(x));
                }
                retVal.Add(mediaList.GetObjectAt(i).GetNamedString("title"),mediaIDs);
            }

            return retVal;
        }

        public List<String> getBoatMedia(String boatID, JesterMediaTypes type)
        {
            List<String> retVal = new List<String>();
            uint index = (uint)type;

            JsonObject obj = this.boatData[boatID].GetObject().GetNamedArray("mediaCollections").GetObjectAt(index);
            JsonArray mediaList = obj.GetNamedArray("media");
            for (uint x = 0; x < mediaList.Count; x++)
            {
                retVal.Add(mediaList.GetStringAt(x));
            }

            return retVal;
        }

        public List<String> getMediaFilesForIDWithExt(String id, String ext)
        {
            List<String> retVal = new List<String>();

            JsonArray arr = this.mediaData[id].GetObject().GetNamedArray("files");

            foreach (JsonValue val in arr)
            {
                String s = val.GetString();
                if (s.EndsWith(ext))
                {
                    //Debug.WriteLine(val.Stringify().ToString());
                    retVal.Add(s);
                }
            }

            return retVal;
        }

        public class BoatData
        {
            [SQLite.PrimaryKey]
            public string _id { get; set; }
            public string doc { get; set; }
        }

        public class MediaData
        {
            [SQLite.PrimaryKey]
            public string _id { get; set; }
            public string doc { get; set; }
        }
    }
}
