﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using System.Diagnostics;
using Windows.UI.Xaml.Media.Imaging;
using System.Collections.ObjectModel;
using Windows.UI;

// The Basic Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234237

namespace Jester
{
    /// <summary>
    /// A basic page that provides characteristics common to most applications.
    /// </summary>
    public sealed partial class DetailsPage : Jester.Common.LayoutAwarePage
    {

        private String boatKey;
        private String boatID;
        private List<String> mediaIDs = new List<String>();
        private int selectedIndex = -43;

        public DetailsPage()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Populates the page with content passed during navigation.  Any saved state is also
        /// provided when recreating a page from a prior session.
        /// </summary>
        /// <param name="navigationParameter">The parameter value passed to
        /// <see cref="Frame.Navigate(Type, Object)"/> when this page was initially requested.
        /// </param>
        /// <param name="pageState">A dictionary of state preserved by this page during an earlier
        /// session.  This will be null the first time a page is visited.</param>
        protected override void LoadState(Object navigationParameter, Dictionary<String, Object> pageState)
        {
            if (pageState != null)
            {
                
                if (pageState.ContainsKey("boatKey"))
                {
                    boatKey = pageState["boatKey"].ToString();
                    pageTitle.Text = boatKey;
                }
                if (pageState.ContainsKey("boatID"))
                {
                    boatID = pageState["boatID"].ToString();
                }
                //order important here because boatID must be defined for a menuselection change event
                if (pageState.ContainsKey("selectedIndex"))
                {
                    selectedIndex = (int)pageState["selectedIndex"];
                    MenuList.SelectedIndex = selectedIndex;
                }
            }
        }

        //Retreive data sent from previous Frame (MainPage)
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            //retrieve title using id sent from previous Frame.

            if (boatKey == null)
            {
                boatKey = e.Parameter as String;
                boatID = DataStore.Instance.getBoats()[boatKey];
                pageTitle.Text = boatKey;
            }
            

            boatKey = e.Parameter as String;
            boatID = DataStore.Instance.getBoats()[boatKey];
            pageTitle.Text = boatKey;

        }

        /// <summary>
        /// Preserves state associated with this page in case the application is suspended or the
        /// page is discarded from the navigation cache.  Values must conform to the serialization
        /// requirements of <see cref="SuspensionManager.SessionState"/>.
        /// </summary>
        /// <param name="pageState">An empty dictionary to be populated with serializable state.</param>
        protected override void SaveState(Dictionary<String, Object> pageState)
        {
            if (selectedIndex != -43)
            {
                pageState["selectedIndex"] = selectedIndex;
            }
            pageState["boatKey"] = boatKey;
            pageState["boatID"] = boatID;
           
        }

        //When a menu item is selected, populate the needed thumbnail gridview 
        private void MenuSelectEvent(object sender, SelectionChangedEventArgs e)
        {

            selectedIndex = MenuList.SelectedIndex;
            DataStore.JesterMediaTypes selected = (DataStore.JesterMediaTypes)MenuList.SelectedIndex;
            //selected will be something like Tour, in boat table in mediaCollections JSON go to title Tour, then grab ids from media table inside there
            Debug.WriteLine("selected is " + selected + " boatID is " + boatID);
            mediaIDs = DataStore.Instance.getBoatMedia(boatID, selected);
           List<Image> ThumbnailSource = new List<Image>();
              for (int i = 0; i < mediaIDs.Count; i++)
              {
                  Image temp = new Image();
                  var uri = new System.Uri("ms-appx:///Assets/media/" + mediaIDs[i] + "/__default.jpg");
                  temp.Source = new BitmapImage(uri);
                  ThumbnailSource.Add(temp);
              }

              //clears all of the items currently shown
              ThumnailList.Items.Clear();

              for (int i = 0; i < ThumbnailSource.Count(); i++)
              {
                  ImageBrush thumnailBrush = new ImageBrush();
                  thumnailBrush.ImageSource = ThumbnailSource.ElementAt(i).Source;

                  GridViewItem thumbnailItem = new GridViewItem
                                               {
                                                   Background = thumnailBrush,
                                                   Width = 254,
                                                   Height = 255,
                                               };

                  ThumnailList.Items.Add(thumbnailItem);
              }

        }

        //When a thumbnail is selected, travel to the correct display page type
        private void ThumbnailSelected(object sender, SelectionChangedEventArgs e)
        {
            int index = ThumnailList.SelectedIndex;
            
            String selected = mediaIDs[index];

            FlipInfo myInfo = new FlipInfo(mediaIDs, index);
            if ((DataStore.JesterMediaTypes)MenuList.SelectedIndex == DataStore.JesterMediaTypes.JMTVideos)
            {
                Frame.Navigate(typeof(VideoContent), selected);
            }
            else
            {
                Frame.Navigate(typeof(ContentDisplay), myInfo);
            }
           
        }
    }
}
