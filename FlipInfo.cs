﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jester
{
    //Class allowing the transmission of both an index integer and a List of strings representing media
    //ID's to different pages.
    class FlipInfo
    {
        private List<String> m_mediaStrings = new List<String>();
        private int m_startIndex;

        public FlipInfo()
        {
            m_startIndex = 0;
        }

        public FlipInfo(List<String> mediaIDs, int index)
        {
            m_mediaStrings = mediaIDs;
            m_startIndex = index;
        }

        public List<String> getMediaStrings()
        {
            return m_mediaStrings;
        }

        public int getStartIndex()
        {
            return m_startIndex;
        }
    }
}
